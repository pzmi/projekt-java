insert into users(name, surname, email, password) values ("Admin", "Admin", "zapisystudia@gmail.com", "CRYPT:zaMRV8JHBwoIo");
INSERT INTO `users` (`id`, `album_number`, `name`, `surname`, `email`, `password`)
            VALUES  (2, 222222, 'pan', 'kracy', 'a@a.a', 'CRYPT:a@vQxjPY0upZk');

insert into roles(role) values("admin");
insert into roles(role) values("user");
insert into roles(role) values("lecturer");
insert into roles(role) values("student");
insert into user_roles values(4, 1);
insert into user_roles values(1, 1);

insert into courses (name, frequency, user_id) VALUES ("Systemy wbudowane", "tydzien", 1);
insert into courses (name, frequency, user_id) VALUES ("Inżynieria programowania", "tydzien", 1);

insert into terms (description, start_date, end_date, capacity, `size`, `group`, courses_id)
          VALUES ("Systemy wbudowane", "2015-02-14 09:15:00", "2015-02-14 10:45:00", 1, 14, 33, 1);
insert into terms (description, start_date, end_date, capacity, `size`, `group`, courses_id)
          VALUES ("Inżynieria Programowania", '2015-02-14 11:00:00', '2015-02-14 12:30:00', 1, 14, 33, 2);

insert into users_terms (user_id, terms_id) values (2,1);
insert into users_terms (user_id, terms_id) values (2,2);
