create table users(id int(16) not null primary key auto_increment, username varchar(16) not null unique key, pwd varchar(255) not null) engine = InnoDB;

create table roles(id int(16) not null primary key auto_increment, role varchar(20) not null unique key) engine = InnoDB;

create table user_roles(user_id int(16) not null, role_id int(16) not null, index(user_id), unique key (user_id, role_id), foreign key(role_id) references roles(id), foreign key(user_id) references users(id))  engine = InnoDB;

