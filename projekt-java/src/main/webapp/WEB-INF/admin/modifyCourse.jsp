<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" var="Text" />
<t:main>
    <jsp:attribute name="title"><fmt:message key="admin.modifycourse" bundle="${Text}"/></jsp:attribute>
    <jsp:attribute name="nav">admin/modifycourse</jsp:attribute>
    <jsp:body>
        <form action="modifycourse" method="post">
            <input id="id" type="hidden" value="${course.id}" name="id">
            <div class="form-group <c:if test='${not empty messages.nameError}'> has-error </c:if>">
                <label for="inputCourseName"><fmt:message key="courseName" bundle="${Text}"/></label>
                <input type="text" class="form-control" name="courseName" id="inputCourseName" value="${fn:escapeXml(course.name)}" placeholder="<fmt:message key="modifycourse.entercoursename" bundle="${Text}"/>">
                <c:if test='${not empty messages.nameError}'>
                    <div class="alert alert-danger" role="alert">${messages.nameError}</div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="inputFrequency"><fmt:message key="frequency" bundle="${Text}"/></label>
                <select name="courseFrequency" id="inputFrequency">
                    <option value="1">Co tydzień</option>
                    <option value="2">Co 2 tygodnie</option>
                    <option value="3">Co miesiąc</option>
                </select>
            </div>
            <div class="form-group">
                <label for="inputLecturerId"><fmt:message key="lecturerId" bundle="${Text}"/></label>
                <select name="lecturerId" id="inputLecturerId">
                    <c:forEach var="element" items="${lecturers}" varStatus="status">
                    <option value=${element.id}>${element.name} ${element.surname}</option>
                    </c:forEach>
                </select>
            </div>

            <button type="submit" class="btn btn-default"><fmt:message key="submit" bundle="${Text}"/></button>
            <a class="btn btn-default" href="courses" role="button"><fmt:message key="cancel" bundle="${Text}"/></a>
        </form>
    </jsp:body>
</t:main>
