<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" var="Text" />

<t:main>
  <jsp:attribute name="title">Informacje o autorach</jsp:attribute>
  <jsp:attribute name="nav">contact</jsp:attribute>
  <jsp:body>
    <div class="jumbotron">

      <h2>Informacje o autorach</h2>
        </div>
      <div class="viewport" align="center">
            <span><b>Patryk Żmigrodzki</b> <br/> Główny zarządzca projektu<br/> Odpowiedzialny za: kierowanie projektem, frontend.
            <br /><a href="mailto:p.zmigrodzki@gmail.com">Napisz wiadomość do Patryka</a>
            </span>
      </div>  <div class=bottom_aligner2></div>
      <div class="viewport" align="center">
            <span><b><br>Maciej Pawlikowski</b>  <br /> Odpowiedzialny za: backend.
            <br /><a href="mailto:bystrzak14@gmail.com">Napisz wiadomość do Macieja</a>
            </span>
      </div> <div class=bottom_aligner2></div>
      <div class="viewport" align="center">
            <span><b><br>Michał Topór-Futer</b>  <br /> Odpowiedzialny za: frontend.
            <br /><a href="mailto:futerzak@gmailcom">Napisz wiadomość do Michała</a>
            </span>
          <div class="viewport" align="center">
        <span><b><br><br>Łukasz Czernik</b>  <br /> Odpowiedzialny za: bazę danych, dokumentację.
            <br /><a href="mailto:l.czernik1992@gmail.com">Napisz wiadomość do Łukasza</a>
            </span>
              </div>
      </div>
  </jsp:body>
</t:main>