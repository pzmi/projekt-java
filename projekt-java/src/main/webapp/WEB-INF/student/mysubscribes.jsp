<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text" var="Text"/>

<t:main>
    <jsp:attribute name="title">Moje zapisy</jsp:attribute>
    <jsp:attribute name="nav">student/mysubscribes</jsp:attribute>
    <jsp:body>
        <div class="jumbotron">
            <h1>Moje zapisy</h1>
        </div>
        <div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>NR.</th>
                    <th>Nazwa przedmiotu</th>
                    <th>Termin</th>
                    <th>Cykliczność</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="i" begin="1" end="10">
                    <tr>
                        <td>id <c:out value="${i}"/></td>
                        <td>Nazwa <c:out value="${i}"/></td>
                        <td>Termin <c:out value="${i}"/></td>
                        <td>Okres <c:out value="${i}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

    </jsp:body>
</t:main>