<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text" var="Text"/>

<t:main>
    <jsp:attribute name="title">Zapis na zajęcia</jsp:attribute>
    <jsp:attribute name="nav">student/coursesubscribe</jsp:attribute>
    <jsp:body>
        <div class="jumbotron">
            <h1>Zapis na zajęcia</h1>
        </div>
        <form method="post" action="coursesubscribe">
            <div class="form-group">
                <label for="courses">Zajęcie</label>
                <select class="form-control" id="courses" name="course">
                    <option value>wybierz</option>
                    <c:forEach var="course" items="${courses}">
                        <option value=
                                <c:out value="${course.id}"/>><c:out value="${course.name}"/></option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label for="terms">Termin</label>
                <select class="form-control" id="terms" name="term">
                    <option value>wybierz</option>
                    <c:forEach var="term" items="${terms}" >
                        <option courseID=<c:out value="${term.course_id}"/> value=
                                <c:out value="${term.id}"/> style="display: none;"><c:out value="${term.start_date}"/></option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <input class="btn btn-default" type="submit" value="Zapisz"/>
            </div>
        </form>
        <script>
            $('#courses').change(function(){
                var courseID=$('#courses').children('option:selected').val();
                $('#terms').children('[courseid]').hide();
                $('#terms').children('[courseid='+courseID+']').show();

            });
        </script>
    </jsp:body>
</t:main>