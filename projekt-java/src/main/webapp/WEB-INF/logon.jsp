<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text" var="Text"/>
<t:main>
    <jsp:attribute name="title">Logon</jsp:attribute>
    <jsp:attribute name="nav">logon</jsp:attribute>
    <jsp:body>
        <h1>Zaloguj się</h1>
        <c:if test='${not empty param["Retry"]}'>
            <span class="error">Incorrect username or password. Please try again.</span>
            <c:set var="errorMessage" value="Incorrect username or password. Please try again."/>
        </c:if>
        <form action="j_security_check" method="POST">
            <p class="form-group">
                <label for="username"><fmt:message key="emailAddress" bundle="${Text}"/></label>
                <input class="form-control" id="username" name="j_username" maxlength="24" type="text"
                       value="${fn:escapeXml(param.name)}">
            </p>

            <p class="form-group">
                <label for="password"><fmt:message key="Password" bundle="${Text}"/></label>
                <input class="form-control" id="password" name="j_password" maxlength="36" type="password"
                       value="${fn:escapeXml(param.age)}">
            </p>

            <p>
                <button type="submit" class="btn btn-default"><fmt:message key="submit" bundle="${Text}"/></button>
            </p>
        </form>
    </jsp:body>
</t:main>
