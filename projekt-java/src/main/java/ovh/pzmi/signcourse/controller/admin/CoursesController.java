package ovh.pzmi.signcourse.controller.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Course;
import ovh.pzmi.signcourse.service.CourseService;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

/**
 * Created by bystrzak on 16.02.15.
 */
@WebServlet(name = "AdminCoursesController",
        urlPatterns = {"admin/courses"})
public class CoursesController extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(CoursesController.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }

        CourseService courseService = new CourseService(dataSource);
        List<Course> courses = courseService.list();

        request.setAttribute("courses", courses);


        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/admin/courses.jsp");
        requestDispatcher.include(request, response);
    }
}
