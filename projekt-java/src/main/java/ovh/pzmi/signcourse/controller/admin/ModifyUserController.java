package ovh.pzmi.signcourse.controller.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Role;
import ovh.pzmi.signcourse.model.User;
import ovh.pzmi.signcourse.model.UserRole;
import ovh.pzmi.signcourse.service.RoleService;
import ovh.pzmi.signcourse.service.UserRoleService;
import ovh.pzmi.signcourse.service.UserService;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created by dexior on 12.02.15.
 */
@WebServlet(name = "ModifyUserController",
            urlPatterns = {"admin/modifyuser"})
public class ModifyUserController extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(ModifyUserController.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", request.getLocale());
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
        Long userId = Long.parseLong(request.getParameter("id"));
        String email = request.getParameter("email");
        int albumNumber = Integer.parseInt(request.getParameter("albumNumber"));
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        
        UserService userService = new UserService(dataSource);
        User user = userService.find(userId);
        user.setEmail(email);
        user.setAlbumNumber(albumNumber);
        user.setName(name);
        user.setSurname(surname);
        userService.update(user);

        UserRoleService userRoleService = new UserRoleService(dataSource);
        List<String> checkedRoles = Arrays.asList(request.getParameterValues("role"));
        List<Long> roleIds = new ArrayList<>();
        for (String checkedRole : checkedRoles) {
            Long roleId = Long.parseLong(checkedRole);
            roleIds.add(roleId);
            UserRole userRole = new UserRole();
            userRole.setRole_id(roleId);
            userRole.setUser_id(userId);
            userRoleService.create(userRole);
        }
        List<UserRole> userRoles = userRoleService.list(userId);
        for (UserRole userRole : userRoles) {
            if (!roleIds.contains(userRole.getRole_id())) {
                userRoleService.delete(userRole);
            }
        }

        List<User> users = userService.list();
        request.setAttribute("users", users);
        request.setAttribute("successMessage", "Zmodyfikowano pomyślnie");
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/admin/users.jsp");
        requestDispatcher.include(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataSource dataSource = null;
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", request.getLocale());
        try {
            InitialContext ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/DS");
        } catch (NamingException e) {
            logger.warn("Datasource lookup failed", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Database error");
        }
        UserService userService = new UserService(dataSource);
        Long userId = Long.parseLong(request.getParameter("id"));
        User user = userService.find(userId);
        if (user == null) {
            request.setAttribute("errorMessage", getUserError(resourceBundle, userId));
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin/users");
            requestDispatcher.forward(request, response);
            return;
        }
        
        request.setAttribute("user", user);
        
        UserRoleService userRoleService = new UserRoleService(dataSource);
        List<UserRole> userRoles = userRoleService.list(user.getId());
        Map<Long, Boolean> roles = new HashMap<>();
        roles.put(1L, false);
        roles.put(2L, false);
        roles.put(3L, false);
        roles.put(4L, false);
        for (UserRole userRole : userRoles) {
            roles.put(userRole.getRole_id(), true);
        }
        request.setAttribute("roles", roles);
        
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/admin/modifyUser.jsp");
        requestDispatcher.include(request, response);
    }
    
    private String getUserError(ResourceBundle resourceBundle, Object... args) {
        MessageFormat formatter = new MessageFormat("{0}");
        formatter.applyPattern(resourceBundle.getString("admin.modifyuser.nosuchuser"));

        String output = formatter.format(args);
        return output;
        
    }
}
