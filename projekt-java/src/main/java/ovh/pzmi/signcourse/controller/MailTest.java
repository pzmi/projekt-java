package ovh.pzmi.signcourse.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ResourceBundle;

/**
 * Created by dexior on 30.01.15.
 */
@WebServlet(name = "MailTest",
            urlPatterns = {"/mail"})
public class MailTest extends HttpServlet {
    final Logger logger = LoggerFactory.getLogger(MailTest.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Session session = null;
        InitialContext ctx = null;
        try {
            ctx = new InitialContext();
            session = (Session) ctx.lookup("java:comp/env/mail/Session");
        } catch (NamingException e) {
            e.printStackTrace();
        }

        if (session == null) {
            logger.error("Failed to initialize mail session");
            return;
        }
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", request.getLocale());

        MimeMessage message = new MimeMessage(session);
        Multipart multipart = new MimeMultipart("alternative");
        BodyPart part1 = new MimeBodyPart();
        try {
            part1.setText(resourceBundle.getString("mail.text"));
            BodyPart part2 = new MimeBodyPart();
            part2.setContent(resourceBundle.getString("mail.html") ,"text/html");
            multipart.addBodyPart(part1);
            multipart.addBodyPart(part2);
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress("dexior11@gmail.com"));
            message.setSubject(resourceBundle.getString("mail.welcome"));
            message.setContent(multipart);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        Transport transport = null;
        try {
            transport = session.getTransport();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }

        if (transport != null) {
            try {
                transport.connect();
                transport.sendMessage(message, message.getAllRecipients());
                transport.close();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }

    }
}
