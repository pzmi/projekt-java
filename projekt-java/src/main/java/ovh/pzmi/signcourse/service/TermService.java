package ovh.pzmi.signcourse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Term;
import ovh.pzmi.signcourse.util.AccountUtils;
import ovh.pzmi.signcourse.util.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dexior on 05.02.15.
 */
public class TermService {
    final private Logger logger = LoggerFactory.getLogger(TermService.class);
    public static final String SQL_FIND_BY_ID = "select * from terms where id = ?";
    public static final String SQL_LIST_ORDER_BY_ID = "select * from terms order by id";
    public static final String SQL_LIST_TERMS_BY_USER_ID = "select distinct * from users_terms left join terms on users_terms.terms_id = terms.id where users_terms.user_id = ? ";
    public static final String SQL_LIST_BY_COURSES_ID = "select * from terms where courses_id = ?";
    public static final String SQL_UPDATE = "update terms set description = ?, start_date = ?, end_date = ?, capacity = ?, `size` = ?, `group` = ?, courses_id = ? where id = ?";
    public static final String SQL_INSERT = "insert into terms(description, start_date, end_date, capacity, `size`, `group`, courses_id) values(?, ?, ?, ?, ?, ?, ?)";
    public static final String SQL_DELETE = "delete from terms where id = ?";
    private DataSource dataSource;

    public TermService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Term find(String sql, Object... values) {
        Term term = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, sql, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            if (resultSet.next()) {
                term = map(resultSet);
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve Term from database.", e);
        }
        return term;
    }

    public Term find(long id) {
        return find(SQL_FIND_BY_ID, id);
    }

    public List<Term> findByUserId(long id) {
        return list(SQL_LIST_TERMS_BY_USER_ID, id);
    }

    public List<Term> listByCourseId(long id){
        return list(SQL_LIST_BY_COURSES_ID, id);
    }

    public List<Term> list(String sql, Object... values) {
        List<Term> terms = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, sql, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                terms.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve Term from database.", e);
        }

        return terms;
    }


    public List<Term> list() {
        List<Term> terms = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                terms.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve Term from database.", e);
        }

        return terms;
    }

    public void create(Term term) {
        if (term.getId() != null) {
            throw new IllegalArgumentException("Term is already created, the term ID is not null.");
        }

        Object[] values = {
            term.getDescription(),
            term.getStart_date(),
            term.getEnd_date(),
            term.getCapacity(),
            term.getSize(),
            term.getGroup(),
            term.getCourse_id()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_INSERT, true, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Creating term failed, no rows affected.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    term.setId(generatedKeys.getLong(1));
                } else {
                    logger.warn("Creating term failed, no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            logger.error("Couldn't insert term");
        }
    }

    public void update(Term term) {
        if (term.getId() == null) {
            throw new IllegalArgumentException("Term is not created yet, the term ID is null.");
        }
        Object[] values = {
                term.getDescription(),
                term.getStart_date(),
                term.getEnd_date(),
                term.getCapacity(),
                term.getSize(),
                term.getGroup(),
                term.getCourse_id(),
                term.getId()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_UPDATE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Updating term failed, no rows affected.");
            }
        } catch (SQLException e) {
            logger.warn("Couldn't update Term.", e);
        }
    }

    public void delete(Term term) {
        Object[] values = {
                term.getId()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_DELETE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Deleting term failed, no rows affected.");
            } else {
                term.setId(null);
            }
        } catch (SQLException e) {
            logger.error("Deleting term failed.", e);
        }
    }

    private Term map(ResultSet resultSet) throws SQLException {
        Term term = new Term();
        term.setId(resultSet.getLong("id"));
        term.setDescription(resultSet.getString("description"));
        term.setStart_date(resultSet.getTimestamp("start_date"));
        term.setEnd_date(resultSet.getTimestamp("end_date"));
        term.setCapacity(resultSet.getInt("capacity"));
        term.setSize(resultSet.getInt("size"));
        term.setGroup(resultSet.getInt("group"));
        term.setCourse_id(resultSet.getLong("courses_id"));
        return term;
    }
}