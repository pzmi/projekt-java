package ovh.pzmi.signcourse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Role;
import ovh.pzmi.signcourse.util.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dexior on 03.02.15.
 */
public class RoleService {
    final private Logger logger = LoggerFactory.getLogger(RoleService.class);
    public static final String SQL_FIND_BY_ID = "select * from roles where id = ?";
    public static final String SQL_LIST_ORDER_BY_ID = "select * from roles order by id";
    public static final String SQL_UPDATE = "update roles set name = ? where id = ?";
    public static final String SQL_INSERT = "insert into roles(name) values(?)";
    public static final String SQL_DELETE = "delete from roles where id = ?";
    private DataSource dataSource;
    
    public RoleService(DataSource dataSource) {
        this.dataSource = dataSource;
        
    }
    
    public Role find(String sql, Object... values) {
        Role role = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, sql, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            if (resultSet.next()) {
                role = map(resultSet);
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve Role from database.", e);
        }
        return role;
    }

    public Role find(long id) {
        return find(SQL_FIND_BY_ID, id);
    }

    public List<Role> list() {
        List<Role> roles = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                roles.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve Roles from database.", e);
        }

        return roles;
    }

    public void create(Role role) {
        if (role.getId() != null) {
            throw new IllegalArgumentException("role is already created, the user ID is not null.");
        }

        Object[] values = {
            role.getName()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_INSERT, true, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Creating role failed, no rows affected.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    role.setId(generatedKeys.getLong(1));
                } else {
                    logger.warn("Creating role failed, no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            logger.error("Couldn't insert role");
        }
    }

    public void update(Role role) {
        if (role.getId() == null) {
            throw new IllegalArgumentException("role is not created yet, the user ID is null.");
        }

        Object[] values = {
                role.getName(),
                role.getId()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_UPDATE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Updating role failed, no rows affected.");
            }
        } catch (SQLException e) {
            logger.warn("Couldn't update role.", e);
        }
    }

    public void delete(Role role) {
        Object[] values = {
                role.getId()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_DELETE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Deleting role failed, no rows affected.");
            } else {
                role.setId(null);
            }
        } catch (SQLException e) {
            logger.error("Deleting role failed.", e);
        }
    }
    
    public Role map(ResultSet resultSet) throws SQLException {
        Role role = new Role();
        role.setId(resultSet.getLong("id"));
        role.setName(resultSet.getString("name"));
        return role;
    }
}
