package ovh.pzmi.signcourse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.Course;
import ovh.pzmi.signcourse.model.UserTerm;
import ovh.pzmi.signcourse.util.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dexior on 03.02.15.
 */
public class CourseService {
    final private Logger logger = LoggerFactory.getLogger(CourseService.class);
    public static final String SQL_FIND_BY_ID = "select * from courses where id = ?";
    public static final String SQL_LIST_ORDER_BY_ID = "select * from courses order by id";
    public static final String SQL_UPDATE = "update courses set name = ?, frequency = ?, user_id = ? where  id = ?";
    public static final String SQL_INSERT = "insert into courses(name, frequency, user_id) values(?, ?, ?)";
    public static final String SQL_DELETE = "delete from courses where id = ?";
    public static final String SQL_FIND_BY_USER_ID = "select * from courses c join users u on c.user_id=u.id where user_id = ?";
    DataSource dataSource;

    public CourseService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Course find(String sql, Object... values) {
        Course course = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, sql, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            if (resultSet.next()) {
                course = map(resultSet);
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve Course from database.", e);
        }
        return course;

    }

    public Course find(long id) {
        return find(SQL_FIND_BY_ID, id);
    }

    public List<Course> list() {
        List<Course> courses = new ArrayList<>();
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                courses.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve Course from database.", e);
        }
        return courses;
    }

    public void create(Course course) {
        if (course.getId() != null) {
            throw new IllegalArgumentException("course is already created, the user ID is not null.");
        }

        Object[] values = {
                course.getName(),
                course.getFrequency(),
                course.getUser_id()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_INSERT, true, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Creating course failed, no rows affected.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    course.setId(generatedKeys.getLong(1));
                } else {
                    logger.warn("Creating course failed, no generated key obtained.");
                }
            }
        } catch (SQLException e) {
            logger.error("Couldn't insert course");
        }
    }

    public void delete(Course course) {
        Object[] values = {
                course.getId()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_DELETE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Deleting course failed, no rows affected.");
            } else {
                course.setId(null);
            }
        } catch (SQLException e) {
            logger.error("Deleting course failed.", e);
        }
    }

    public void update(Course course) {
        if (course.getId() == null) {
            throw new IllegalArgumentException("course is not created yet, the course ID is null.");
        }
        String frequency = course.getFrequency().toString();
        Object[] values = {
                course.getName(),
                frequency,
                course.getUser_id(),
                course.getId()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_UPDATE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Updating course failed, no rows affected.");
            }
        } catch (SQLException e) {
            logger.warn("Couldn't update course.", e);
        }
    }

    private Course map(ResultSet resultSet) throws SQLException {
        Course course = new Course();
        course.setId(resultSet.getLong("id"));
        course.setName(resultSet.getString("name"));
        course.setFrequency(Course.Frequency.valueOf(resultSet.getString("frequency")));
        course.setUser_id(resultSet.getLong("user_id"));
        return course;
    }
    public List<Course> listByUserID(Long userId) {
        List<Course> courses = new ArrayList<>();
        Object[] values = {
                userId
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_FIND_BY_USER_ID, false, values);
                ResultSet resultSet = statement.executeQuery()
        ) {
            while (resultSet.next()) {
                courses.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve Courses from database.", e);
        }

        return courses;
    }
}
