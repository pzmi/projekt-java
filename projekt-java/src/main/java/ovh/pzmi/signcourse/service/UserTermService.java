package ovh.pzmi.signcourse.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ovh.pzmi.signcourse.model.UserTerm;
import ovh.pzmi.signcourse.util.AccountUtils;
import ovh.pzmi.signcourse.util.DbUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dexior on 05.02.15.
 */
public class UserTermService {
    final private Logger logger = LoggerFactory.getLogger(UserTermService.class);
    public static final String SQL_FIND_BY_ID = "select * from users_terms where user_id = ? and terms_id = ?";
    public static final String SQL_LIST_ORDER_BY_ID = "select * from users_terms order by user_id, terms_id";
    public static final String SQL_LIST_BY_ID = "select * from users_terms natural join users where terms_id = ?";
    public static final String SQL_UPDATE = "update users_terms set is_reserved = ? where user_id = ? and terms_id = ?";
    public static final String SQL_INSERT = "insert ignore into users_terms(user_id, terms_id, is_reserved) values(?, ?, ?)";
    public static final String SQL_DELETE_BY_USER_ID_POSITION_ID = "delete from users_terms where user_id = ? and terms_id = ?";
    private DataSource dataSource;

    public UserTermService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public UserTerm find(String sql, Object... values) {
        UserTerm userTerm = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, sql, false, values);
                ResultSet resultSet = statement.executeQuery();
        ) {
            if (resultSet.next()) {
                userTerm = map(resultSet);
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve UserTerm from database.", e);
        }
        return userTerm;
    }

    public UserTerm find(long id) {
        return find(SQL_FIND_BY_ID, id);
    }

    public List<UserTerm> list() {
        List<UserTerm> userTerms = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_LIST_ORDER_BY_ID);
                ResultSet resultSet = statement.executeQuery();
        ) {
            while (resultSet.next()) {
                userTerms.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve UserTerm from database.", e);
        }

        return userTerms;
    }

    public List<UserTerm> list(Long termsId) {
        List<UserTerm> userTerms = new ArrayList<>();
        Object[] values = {
            termsId
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_LIST_BY_ID, false, values);
                ResultSet resultSet = statement.executeQuery()
        ) {
            while (resultSet.next()) {
                userTerms.add(map(resultSet));
            }
        } catch (SQLException e) {
            logger.error("Couldn't retrieve UserTerms from database.", e);
        }

        return userTerms;
    }

    public void create(UserTerm userTerm) {


        Object[] values = {
            userTerm.getUser_id(),
                userTerm.getTerm_id(),
                userTerm.isReserve()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_INSERT, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Creating userTerm failed, no rows affected.");
            }

        } catch (SQLException e) {
            logger.error("Couldn't insert userTerm");
        }
    }

    public void update(UserTerm userTerm) {
        if (userTerm.getUser_id() == null || userTerm.getTerm_id() == null) {
            throw new IllegalArgumentException("UserTerm is not created yet, the userTerm ID is null.");
        }

        Object[] values = {
            userTerm.isReserve(),
                userTerm.getUser_id(),
                userTerm.getTerm_id()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_UPDATE, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Updating userTerm failed, no rows affected.");
            }
        } catch (SQLException e) {
            logger.warn("Couldn't update UserTerm.", e);
        }
    }

    public void delete(UserTerm userTerm) {
        Object[] values = {
                userTerm.getUser_id(),
                userTerm.getTerm_id()
        };

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = DbUtils.prepareStatement(connection, SQL_DELETE_BY_USER_ID_POSITION_ID, false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                logger.warn("Deleting userTerm failed, no rows affected.");
            } else {
                userTerm.setUser_id(null);
                userTerm.setTerm_id(null);
            }
        } catch (SQLException e) {
            logger.error("Deleting userTerm failed.", e);
        }
    }

    private UserTerm map(ResultSet resultSet) throws SQLException {
        UserTerm userTerm = new UserTerm();
        userTerm.setUser_id(resultSet.getLong("user_id"));
        userTerm.setTerm_id(resultSet.getLong("term_id"));
        userTerm.setReserve((resultSet.getBoolean("is_reserved")));
        return userTerm;
    }
}